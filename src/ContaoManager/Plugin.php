<?php
/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MetatagsBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Memo\MetatagsBundle\MetatagsBundle;
use Contao\CalendarBundle\ContaoCalendarBundle;
use Contao\NewsBundle\ContaoNewsBundle;
use Terminal42\ChangeLanguage;

class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
		return [
			BundleConfig::create(MetatagsBundle::class)->setLoadAfter([
				ContaoCoreBundle::class,
				ContaoNewsBundle::class,
				ContaoCalendarBundle::class,
				ChangeLanguage::class
			])
		];
    }
}
