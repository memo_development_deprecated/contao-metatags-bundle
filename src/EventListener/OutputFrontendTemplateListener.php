<?php

/**
 * @package   MetatagsBundle
 * @author	Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MetatagsBundle\EventListener;

use Contao\CalendarEventsModel;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\Input;
use Contao\PageModel;
use Contao\UserModel;

/**
 * @Hook("outputFrontendTemplate")
 */
class OutputFrontendTemplateListener
{
	public function __invoke(string $strContent, string $strTemplate): string
	{

		// Is it the fe_page or a own version of it? (only act on those root templates)
		if (stristr($strTemplate, 'fe_page'))
		{
			$arrOGTags = array(
				'og:title' => 'ogTitle',
				'og:description' => 'ogDescription',
				'og:type' => 'ogType',
				'og:url' => 'ogUrl',
				'og:image' => 'ogImage',
				'og:determiner' => 'ogDeterminer',
				'og:locale' => 'ogLocale',
				'og:locale:alternate' => 'auto',
				'og:site_name' => 'rootPageTitle',
				'og:video' => 'ogVideo',
				'article:published_time' => 'ogArticlePublishedtime',
				'article:modified_time' => 'ogArticleModifiedtime',
				'article:expiration_time' => 'ogArticleExpirationtime',
				'article:author' => 'ogArticleAuthor',
				'article:section' => 'ogArticleSection',
				'article:tag' => 'ogArticleTag',
				'event:start_time' => 'ogEventStarttime',
				'event:location' => 'ogLocation'
			);
			
			$strAbsoluteURL = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

			// Get the current page
			global $objPage;

			// Loop all tags and fill with values
			foreach($arrOGTags as $strTag => $strValue){

				switch ($strTag){
					case 'og:title':
						if($objPage->$strValue == '' && $objPage->pageTitle != ''){
							$arrOGTags[$strTag] = $objPage->pageTitle;
						} elseif($objPage->$strValue == '' && $objPage->pageTitle == ''){
							$arrOGTags[$strTag] = $objPage->title;
						} else {
							$arrOGTags[$strTag] = $objPage->$strValue;
						}
						break;
					case 'og:description':
						if($objPage->$strValue == '' && $objPage->description != ''){
							$arrOGTags[$strTag] = $objPage->description;
						} else {
							$arrOGTags[$strTag] = $objPage->$strValue;
						}
						break;
					case 'og:url':
						if($objPage->$strValue == ''){
							$arrOGTags[$strTag] = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
						} else {
							$arrOGTags[$strTag] = $objPage->$strValue;
						}
						break;
					case 'og:image':
						if($objPage->ogImage != '' && $objPage->ogImage && $objImage = \FilesModel::findByUuid($objPage->ogImage)){
							$arrOGTags[$strTag] = $strAbsoluteURL. '/' . $objImage->path;
						} elseif($objPage->ogImage != ''){
							if(stristr($objPage->ogImage, 'http')){
								$arrOGTags[$strTag] = $objPage->ogImage;
							} else {
								$arrOGTags[$strTag] = $strAbsoluteURL. '/' . $objPage->ogImage;
							}
						} else {
							unset($arrOGTags[$strTag]);
						}
						break;
					case 'og:locale':
						if($colRootPage = PageModel::findPublishedRootPages()){

							// Get all Rootpages and loop them (looking for locale-tags)
							$arrLocales = array();
							foreach($colRootPage as $objRootPage){
								if($objRootPage->id == $objPage->rootId && isset($objRootPage->ogLocale)){
									$objRootPageMain = $objRootPage;
									$arrOGTags[$strTag] = $objRootPage->ogLocale;
								}elseif (isset($objRootPage->ogLocale)){
									$arrLocales[] = $objRootPage->ogLocale;
								}
							}

							// Check if other languages exist
							if(count($arrLocales) > 0){
								$arrOGTags['og:locale:alternate'] = implode(',', $arrLocales);
							} else {
								unset($arrOGTags['og:locale:alternate']);
							}

						} else {
							unset($arrOGTags[$strTag]);
							unset($arrOGTags['og:locale:alternate']);
						}
					case 'og:locale:alternate':
						break;
					default:
						if($objPage->$strValue && $objPage->$strValue != ''){
							$arrOGTags[$strTag] = $objPage->$strValue;
						} else {
							unset($arrOGTags[$strTag]);
						}
						break;
				}
			}

			// Handle Events - check if event is set
			$strEventAlias = Input::get('events');
			if($strEventAlias && $objEvent = CalendarEventsModel::findOneByAlias($strEventAlias)){

				// og:title
				if(isset($objEvent->ogTitle)){
					$arrOGTags['og:title'] = $objEvent->ogTitle;
				}

				// og:description
				if(isset($objEvent->ogDescription)){
					$arrOGTags['og:description'] = $objEvent->ogDescription;
				}

				// og:type
				if(isset($objEvent->ogType)){
					$arrOGTags['og:type'] = $objEvent->ogType;
				}

				// Check if we can overwrite the image
				if($objImage = \FilesModel::findByUuid($objEvent->singleSRC)){

					$strImagePath = $objImage->path;

					if($strImagePath != ''){
						$arrOGTags['og:image'] = $strImagePath;
					}
				}

				// event:start_time
				$arrOGTags['event:start_time'] = date("Y-m-d\TH:i:s\Z", $objEvent->startTime);

				// event:location
				if(isset($objEvent->location)){
					$arrOGTags['event:location'] = $objEvent->location;
				}

			}

			// HOOK: You can change the array with tag => value here, before we add it to the head-section
			if (isset($GLOBALS['TL_HOOKS']['generateOGTags']) && \is_array($GLOBALS['TL_HOOKS']['generateOGTags']))
			{
				foreach ($GLOBALS['TL_HOOKS']['generateOGTags'] as $callback)
				{
					$this->import($callback[0]);
					$arrOGTags = $this->{$callback[0]}->{$callback[1]}($arrOGTags, $objPage, $objRootPageMain);
				}
			}

			// Add all the OG-Tags to the head-section of the current page
			foreach($arrOGTags as $strTag => $strValue){
				if(isset($strValue)){
					$GLOBALS['TL_HEAD'][] = '<meta property="'.$strTag.'" content="'.$strValue.'" />';
				}
			}
		}

		// Return unchanged Content
		return $strContent;
	}
}
