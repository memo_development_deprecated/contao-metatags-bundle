<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MetatagsBundle\EventListener;

use Contao\Controller;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Contao\FrontendTemplate;
use Contao\Module;
use Contao\UserModel;

/**
 * @Hook("parseArticles")
 */
class ParseArticlesListener
{
	public function __invoke(FrontendTemplate $template, array $newsEntry, Module $module): void
	{
		// Only act on reader modules
		if(stristr($module->type, 'reader')){

			// Get global Page object
			global $objPage;

			// og:title
			if(isset($newsEntry['ogTitle'])){
				$objPage->ogTitle = $newsEntry['ogTitle'];
			}

			// og:description
			if(isset($newsEntry['ogDescription'])){
				$objPage->ogDescription = $newsEntry['ogDescription'];
			}

			// og:type
			if(isset($newsEntry['ogType'])){
				$objPage->ogType = $newsEntry['ogType'];
			}

			// Check if we can overwrite the image
			if($objImage = \FilesModel::findByUuid($newsEntry['singleSRC'])){

				$strImagePath = $objImage->path;

				if($strImagePath != ''){
					$objPage->ogImage = $strImagePath;
				}
			}

			// article:published_time
			$objPage->ogArticlePublishedtime = date("Y-m-d\TH:i:s\Z", $newsEntry['date']);

			// article:modified_time
			$objPage->ogArticleModifiedtime = date("Y-m-d\TH:i:s\Z", $newsEntry['tstamp']);

			// article:expiration_time
			$objPage->ogArticleExpirationtime = date("Y-m-d\TH:i:s\Z", $newsEntry['ogExpiration']);

			// article:author
			$objPage->ogArticleAuthor = UserModel::findByPk($newsEntry['author'])->name;

			// article:section
			if(isset($newsEntry['ogSection'])){
				$objPage->ogArticleSection = $newsEntry['ogSection'];
			}

			// article:tag
			if(isset($newsEntry['ogTag'])){
				$objPage->ogArticleTag = $newsEntry['ogTag'];
			}
		}
	}
}
