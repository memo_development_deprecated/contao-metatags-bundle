<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MetatagsBundle\EventListener\DataContainer;

use Contao\DataContainer;
use Contao\CoreBundle\ServiceAnnotation\Callback;
use Contao\PageModel;

class CallbackListener
{
	/**
	 * @Callback(table="tl_page", target="fields.ogHelp.input_field")
	 */
	public function onInputFieldCallback(DataContainer $dc, string $strLabel)
	{
		return "<div class='long clr' style='margin-left: 15px; margin-right: 15px; position: relative; height: auto; width: calc(100% - 30px);'>".$GLOBALS['TL_LANG']['tl_page']['ogHelp']."</div>";
	}
}
