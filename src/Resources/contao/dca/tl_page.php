<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\DataContainer;

PaletteManipulator::create()
	->addLegend('extended_meta_legend', 'meta_legend', PaletteManipulator::POSITION_AFTER)
	->addField('ogHelp', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->addField('ogTitle', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->addField('ogDescription', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->addField('ogType', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->addField('ogDeterminer', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->addField('ogVideo', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->addField('ogImage', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
	->applyToPalette('regular', 'tl_page');

PaletteManipulator::create()
	->addLegend('extended_meta_legend', 'meta_legend', PaletteManipulator::POSITION_AFTER)
	->addField('ogLocale', 'language', PaletteManipulator::POSITION_AFTER)
	->applyToPalette('root', 'tl_page');

PaletteManipulator::create()
	->addLegend('extended_meta_legend', 'meta_legend', PaletteManipulator::POSITION_AFTER)
	->addField('ogLocale', 'language', PaletteManipulator::POSITION_AFTER)
	->applyToPalette('rootfallback', 'tl_page');

PaletteManipulator::create()
	->addLegend('extended_meta_legend', 'meta_legend', PaletteManipulator::POSITION_AFTER)
	->addField('ogLocale', 'language', PaletteManipulator::POSITION_AFTER)
	->applyToPalette('default', 'tl_page');

$GLOBALS['TL_DCA']['tl_page']['fields']['ogHelp'] = array
(
	'exclude'                 => true,
	'eval'                    => array(
		'tl_class' => 'long clr'
	)
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogTitle'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogTitle'],
	'exclude'                 => true,
	'filter'                  => false,
	'search'                  => true,
	'sorting'                 => false,
	'inputType'               => 'text',
	'eval'                    => array(
		'mandatory'=> false,
		'tl_class' => 'long clr'
	),
	'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogDescription'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogDescription'],
	'exclude'                 => true,
	'filter'                  => false,
	'search'                  => true,
	'sorting'                 => false,
	'inputType'               => 'textarea',
	'eval'                    => array(
		'mandatory' => false,
		'tl_class' => 'long clr',
		'cols' => 4
	),
	'sql'                     => ['type' => 'text', 'notnull' => false]
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogType'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogType'],
	'exclude'                 => true,
	'default'                 => 'website',
	'filter'                  => false,
	'search'                  => true,
	'sorting'                 => false,
	'inputType'               => 'text',
	'eval'                    => array(
		'mandatory'=> false,
		'tl_class' => 'w50 clr',
		'nospace' => true,
	),
	'sql'                     => ['type' => 'string', 'length' => 255, 'default' => 'website']
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogImage'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogImage'],
	'exclude'                 => true,
	'filter'                  => false,
	'search'                  => false,
	'sorting'                 => false,
	'inputType'               => 'fileTree',
	'eval'                    => array(
		'mandatory'=> false,
		'filesOnly'=> true,
		'extensions'=>Config::get('validImageTypes'),
		'fieldType'=>'radio',
		'tl_class' => 'long clr'
	),
	'sql'                     => ['type' => 'binary', 'length' => 16, 'notnull' => false]
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogDeterminer'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogDeterminer'],
	'exclude'                 => true,
	'filter'                  => false,
	'search'                  => true,
	'sorting'                 => false,
	'inputType'               => 'text',
	'eval'                    => array(
		'mandatory'=> false,
		'tl_class' => 'w50'
	),
	'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogVideo'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogVideo'],
	'exclude'                 => true,
	'filter'                  => false,
	'search'                  => true,
	'sorting'                 => false,
	'inputType'               => 'text',
	'eval'                    => array(
		'mandatory'=> false,
		'tl_class' => 'long clr',
		'nospace' => true,
	),
	'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
);

$GLOBALS['TL_DCA']['tl_page']['fields']['ogLocale'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_page']['ogLocale'],
	'exclude'                 => true,
	'filter'                  => false,
	'search'                  => true,
	'sorting'                 => false,
	'inputType'               => 'text',
	'eval'                    => array(
		'mandatory'=> false,
		'tl_class' => 'w50',
		'nospace' => true,
	),
	'sql'                     => ['type' => 'string', 'length' => 8, 'notnull' => false]
);
