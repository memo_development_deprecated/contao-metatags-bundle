<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\DataContainer;

// Only act, if tl_news is loaded
if(isset($GLOBALS['TL_DCA']['tl_news'])){

	PaletteManipulator::create()
		->addLegend('extended_meta_legend', 'meta_legend', PaletteManipulator::POSITION_AFTER)
		->addField('ogTitle', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogDescription', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogType', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogDeterminer', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogVideo', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogImage', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogExpiration', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogTag', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogSection', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->applyToPalette('default', 'tl_news');

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogHelp'] = array
	(
		'exclude'                 => true,
		'eval'                    => array(
			'tl_class' => 'long clr'
		)
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogTitle'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogTitle'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'long clr'
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogDescription'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogDescription'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'textarea',
		'eval'                    => array(
			'mandatory' => false,
			'tl_class' => 'long clr',
			'cols' => 4
		),
		'sql'                     => ['type' => 'text', 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogType'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogType'],
		'exclude'                 => true,
		'default'                 => 'article',
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'w50 clr',
			'nospace' => true,
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'default' => 'article ']
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogImage'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogImage'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => false,
		'sorting'                 => false,
		'inputType'               => 'fileTree',
		'eval'                    => array(
			'mandatory'=> false,
			'filesOnly'=> true,
			'extensions'=>Config::get('validImageTypes'),
			'fieldType'=>'radio',
			'tl_class' => 'long clr'
		),
		'sql'                     => ['type' => 'binary', 'length' => 16, 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogVideo'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogVideo'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'long clr',
			'nospace' => true,
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogExpiration'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogExpiration'],
		'exclude'                 => true,
		'filter'                  => false,
		'default'                 => time(),
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'long clr',
			'nospace' => true,
			'rgxp'=>'date',
			'datepicker'=>true
		),
		'load_callback' => array
		(
			array('tl_news', 'loadDate')
		),
		'sql'                     => ['type' => 'integer', 'length' => 10, 'notnull' => true, 'default'=>0]
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogSection'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogSection'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'w50 clr',
			'nospace' => true,
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_news']['fields']['ogTag'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_news']['ogTag'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'w50 clr',
			'nospace' => true,
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
	);

}
