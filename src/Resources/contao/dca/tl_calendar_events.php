<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\DataContainer;

// Only act, if tl_calendar_events is loaded
if(isset($GLOBALS['TL_DCA']['tl_calendar_events'])){

	PaletteManipulator::create()
		->addLegend('extended_meta_legend', 'meta_legend', PaletteManipulator::POSITION_AFTER)
		->addField('ogTitle', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogType', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogDescription', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogDeterminer', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogVideo', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->addField('ogImage', 'extended_meta_legend', PaletteManipulator::POSITION_APPEND)
		->applyToPalette('default', 'tl_calendar_events');

	$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ogHelp'] = array
	(
		'exclude'                 => true,
		'eval'                    => array(
			'tl_class' => 'long clr'
		)
	);

	$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ogTitle'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_calendar_events']['ogTitle'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'w50 clr'
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ogDescription'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_calendar_events']['ogDescription'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'textarea',
		'eval'                    => array(
			'mandatory' => false,
			'tl_class' => 'long clr',
			'cols' => 4
		),
		'sql'                     => ['type' => 'text', 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ogType'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_calendar_events']['ogType'],
		'exclude'                 => true,
		'default'                 => 'event',
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'w50',
			'nospace' => true,
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'default' => 'event ']
	);

	$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ogImage'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_calendar_events']['ogImage'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => false,
		'sorting'                 => false,
		'inputType'               => 'fileTree',
		'eval'                    => array(
			'mandatory'=> false,
			'filesOnly'=> true,
			'extensions'=>Config::get('validImageTypes'),
			'fieldType'=>'radio',
			'tl_class' => 'long clr'
		),
		'sql'                     => ['type' => 'binary', 'length' => 16, 'notnull' => false]
	);

	$GLOBALS['TL_DCA']['tl_calendar_events']['fields']['ogVideo'] = array
	(
		'label'                   => &$GLOBALS['TL_LANG']['tl_calendar_events']['ogVideo'],
		'exclude'                 => true,
		'filter'                  => false,
		'search'                  => true,
		'sorting'                 => false,
		'inputType'               => 'text',
		'eval'                    => array(
			'mandatory'=> false,
			'tl_class' => 'long clr',
			'nospace' => true,
		),
		'sql'                     => ['type' => 'string', 'length' => 255, 'notnull' => false]
	);

}
