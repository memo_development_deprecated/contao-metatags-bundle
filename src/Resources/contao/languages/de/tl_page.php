<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_page']['extended_meta_legend'] = 'Erweiterte Metadaten';


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_page']['ogImage'] = array( 'Seitenbild', 'Bild jenes als Open Graph Bild ausgegeben wird und so z.B. von Facebook, Twitter, etc. als Seitenbild verwendet wird, wenn man den Link postet.' );
$GLOBALS['TL_LANG']['tl_page']['ogTitle'] = array( 'Seitentitel', 'Wenn nicht definiert, wird zuerst der "Seitentitel" in den Metadaten verwendet oder falls der auch nicht definiert ist, der "Seitenname".' );
$GLOBALS['TL_LANG']['tl_page']['ogDescription'] = array( 'Beschreibung der Seite', 'Wenn nicht definiert, wird zuerst die "Beschreibung der Seite" in den Metadaten verwendet.' );
$GLOBALS['TL_LANG']['tl_page']['ogType'] = array( 'Seitentyp', "Standardmässig 'website'. Optional Wert von hier: <a href='https://ogp.me/#types' target='_blank'>https://ogp.me/#types</a> verwenden" );
$GLOBALS['TL_LANG']['tl_page']['ogDeterminer'] = array( 'Bestimmungswort', "Wort jenes in einem Satz vor dem Titel eingefügt wird. Mehr dazu hier: <a href='https://ogp.me/#optional' target='_blank'>https://ogp.me/#optional</a>" );
$GLOBALS['TL_LANG']['tl_page']['ogVideo'] = array( 'Video-URL', 'URL zu einem Video, jenes die Seite beschreibt oder dazu passt.' );
$GLOBALS['TL_LANG']['tl_page']['ogLocale'] = array( 'Sprache & Land', 'Kürzel der Sprache und des Landes im Format language_TERRITORY z.B. de_CH oder en_GB, etc. ' );


/**
 * Help-Texts
 */
$GLOBALS['TL_LANG']['tl_page']['ogHelp'] = '<h3 style="margin-bottom: 5px;">Erklärung</h3><p style="line-height: 1.25;">Dieser Abschnitt ermöglicht die gezielte Erfassung von sogenannten <strong>OG (Open Graph) Tags</strong>.<br>Diese Tags sind standardisiert und hier ausführlicher erklärt: <a href="https://ogp.me/" target="_blank">https://ogp.me/</a><br><br>Diese werden vor allem von grossen sozialen Netzwerken (Facebook, Twitter, etc.) verwendet, um Zusatzinformationen einer Webseite zu erfassen, beim teilen eins Links. Sprich wenn Sie hier ein Bild auswählen und die Seite dann bei Facebook posten, sollte Facebook im Normalfall dieses Bild erkennen und als Vorschaubild dieser Webseite anzeigen.<br><br>Falls das nicht der Fall ist, kann es sein, dass man den Cache bei Facebook leeren muss:<br><a href="https://developers.facebook.com/tools/debug/" target="_blank">https://developers.facebook.com/tools/debug/</a></p>';
