<?php

/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


/**
 * Legend
 */
$GLOBALS['TL_LANG']['tl_calendar_events']['extended_meta_legend'] = 'Erweiterte Metadaten';


/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_calendar_events']['ogImage'] = array( 'Seitenbild', 'Bild jenes als Open Graph Bild ausgegeben wird und so z.B. von Facebook, Twitter, etc. als Seitenbild verwendet wird, wenn man den Link postet.' );
$GLOBALS['TL_LANG']['tl_calendar_events']['ogTitle'] = array( 'Seitentitel', 'Wenn nicht definiert, wird zuerst der "Seitentitel" in den Metadaten verwendet oder falls der auch nicht definiert ist, der "Seitenname".' );
$GLOBALS['TL_LANG']['tl_calendar_events']['ogDescription'] = array( 'Beschreibung der Seite', 'Wenn nicht definiert, wird zuerst die "Beschreibung der Seite" in den Metadaten verwendet.' );
$GLOBALS['TL_LANG']['tl_calendar_events']['ogType'] = array( 'Seitentyp', "Standardmässig 'website'. Optional Wert von hier: <a href='https://ogp.me/#types' target='_blank'>https://ogp.me/#types</a> verwenden" );
$GLOBALS['TL_LANG']['tl_calendar_events']['ogDeterminer'] = array( 'Bestimmungswort', "Wort jenes in einem Satz vor dem Titel eingefügt wird. Mehr dazu hier: <a href='https://ogp.me/#optional' target='_blank'>https://ogp.me/#optional</a>" );
$GLOBALS['TL_LANG']['tl_calendar_events']['ogVideo'] = array( 'Video-URL', 'URL zu einem Video, jenes die Seite beschreibt oder dazu passt.' );
