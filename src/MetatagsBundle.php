<?php
/**
 * @package   MetatagsBundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\MetatagsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MetatagsBundle extends Bundle
{
}
